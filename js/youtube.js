const downloadServerURL = getDownloadServerURL();

const formatMovie = "movie";
const formatMusic = "music";


/**
 * youtubeの動画をダウンロードさせる
 */

/**
 * youtubeの動画をダウンロードさせる
 * @param {string} url 
 * @param {string} format : movie or music
 */
function download(url, format) {

    // check
    if (!isValidURL(url)) {
        alert("URLを入力してください");
        errorLoading();
        return;
    }

    if (!isValidFormat(format)) {
        alert("Formatを選択してください");
        errorLoading();
        return;
    }



    let youtubeID = getYoutubeID(url);

    // get movie path
    let requestURL;
    switch (format) {
        case formatMovie:
            requestURL = downloadMovieInfoURL(youtubeID);
            break;
        case formatMusic:
            requestURL = downloadMusicInfoURL(youtubeID);
            break;
        default:
            alert("format error...");
            errorLoading();
            return;
    }

    // download movie
    let callback = function (xhr) {

        let status = xhr.status;
        let responseText = xhr.responseText;

        // json parse
        let response;
        try {
            response = jsonParse(responseText);
        } catch (e) {
            alert("Error:", e);
            errorLoading();
            return;
        }

        if (!response.success) {
            alert("youtubeの情報の取得に失敗しました");
            errorLoading();
            return;
        }

        let result = response.result;

        // download movie
        downloadFromServer(result.title, result.download_path, result.suffix);
    };


    // get userid // todo keyをconstantにする
    let userID = sessionStorage.getItem("youtube-dl-userid");
    let token = sessionStorage.getItem("youtube-dl-token");

    // get request 
    getAjax(requestURL, callback, userID, token);

}

/**
 * 動画ファイルダウンロードプロセス
 * @param {string} title 動画ファイルのタイトル
 * @param {string} download_path 動画ファイルのダウンロードURL
 * @param {string} suffix 動画ファイルの拡張子
 */
function downloadFromServer(title, download_path, suffix) {

    // make download url
    let downloadURL = downloadMovieURL(download_path);

    // filaName
    let fileName = title + "." + suffix; // aaa.mp4

    let uri = downloadURL + "?name=" + fileName;

    // ダウンロード
    let xhr = new XMLHttpRequest(),
        a = document.createElement('a'),
        file;

    xhr.open('GET', uri, true);
    xhr.responseType = 'blob';
    xhr.onload = function () {
        file = new Blob([this.response], { type: "application/octet-stream" });
        a.href = window.URL.createObjectURL(file);
        a.download = fileName;
        a.click();
        alert(`${fileName}をダウンロードしています`);

        // loading css stop
        stopLoading();
    };

    // start
    xhr.send();
}


/**
 * 実際の動画をダウンロードできるURL
 * @param {string} path -> こんなやつ download/VGhpbmtpbmcgLSBOb3JyYm90dGVuIEJpZyBCYW5kIGFuZCBLTk9XRVI=.mp4
 * @return {string} url
 */
function downloadMovieURL(path) {
    // make url
    return downloadServerURL + "/" + path;
}


/**
 * 動画ファイルのダウンロード方法が取得できるpathを取得する
 * @param {string} youtubeID
 * 
 * @return {string} downloadURL
 */
function downloadMovieInfoURL(youtubeID) {
    // make url
    return downloadServerURL + "/movie/download/" + youtubeID;
}

/**
 * 音楽ファイルのダウンロード方法が取得できるpathを取得する
 * @param {string} youtubeID 
 * 
 * @return {string} downloadURL
 */
function downloadMusicInfoURL(youtubeID) {
    return downloadServerURL + "/music/download/" + youtubeID;
}

/**
 * Jsonをparseする
 * @param {string} string 
 * 
 * @return {string}
 */
function jsonParse(string) {
    return JSON.parse(string);
}



/**
 * URLからyoutubeのidを取得する、以下の2パターンの抽出が可能
 * https://www.youtube.com/watch?v=_AtSzBrW6mo
 * https://youtu.be/_AtSzBrW6mo
 * 
 * @param {string} url 
 */
function getYoutubeID(url) {

    const domain = getDomain(url);

    switch (domain) {
        case "www.youtube.com":
            let indexParam = url.indexOf("?");
            let paramStr = url.slice(indexParam)
            let urlParams = new URLSearchParams(paramStr);
            return urlParams.get('v');
        case "youtu.be":
            let urlSplit = url.split("/");
            let splitLength = urlSplit.length;
            return urlSplit[splitLength - 1];
        default:
            alert("URL Error: youtubeのURLではありません");
            return;
    }
}

/**
 * urlからdomainを取得する
 * @param {string} url 
 */
function getDomain(url) {
    return url.match(/^https?:\/{2,}(.*?)(?:\/|\?|#|$)/)[1];
}

/**
 * URLかどうかを判定
 * true: URL
 * false: URLではない
 * @param {string} string 
 * @returns {boolean}
 */
function isValidURL(string) {
    var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    if (res == null)
        return false;
    else
        return true;
};

/**
 * 指定された拡張子かどうかを確認
 * true: ok
 * false: error
 * @param {string} string 
 * @returns {boolean} 
 */
function isValidFormat(string) {
    return string === formatMovie || string === formatMusic;
}