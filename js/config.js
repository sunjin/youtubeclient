

/**
 * 自分のURLを確認して、DownloadServerのURLを取得する
 */
function getDownloadServerURL() {
    let myHost = location.hostname;

    switch (myHost) {
        case "127.0.0.1":
        case "localhost":
            return "http://localhost:9000";
        default:
            return "https://api.sunjin.info";
    }
}
