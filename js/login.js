const serverURL = getDownloadServerURL();

/**
 * Login Event
 */
function onClickLogin() {

    console.log("===== login =====")

    let user = document.getElementById('user').value;
    let password = document.getElementById('password').value;

    login(user, password);

    return false;

}

/**
 * 実際のlogin処理
 * @param {string} user 
 * @param {string} pass 
 */
function login(user, pass) {

    // todo chck user, pass empty

    let data = { user: user, pass: pass };

    let url = serverURL + "/login";

    let callback = function (xhr) {

        let responseText = xhr.responseText;

        let response;
        try {
            response = jsonParse(responseText);
        } catch (e) {
            alert("Error:", e);
            return;
        }

        if (!response.success) {
            alert("Loginに失敗しました");
            return;
        }

        // session storageにtokenとuserIDを保存する
        if (('sessionStorage' in window) && (window.sessionStorage !== null)) {
            sessionStorage.setItem("youtube-dl-token", response.result.token);
            sessionStorage.setItem("youtube-dl-userid", user);
        } else {
            alert("SessionStorageが使えません、違うブラウザを試してください");
            return;
        }

        // jump top
        location.assign('index.html');
    }

    postAjax(url, toJson(data), callback, "", "true")
}

/**
 * jsonにする
 * @param {*} data 
 */
function toJson(data) {
    return JSON.stringify(data);
}

function jsonParse(string) {
    return JSON.parse(string);
}