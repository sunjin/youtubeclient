
/**
 * 描画前のEvent
 */
window.addEventListener('DOMContentLoaded', function () {
    this.checkToken();
});

/**
 * Token確認
 */
function checkToken() {
    let token = sessionStorage.getItem('youtube-dl-token');

    if (token == null) {
        // go login.html
        location.assign('login.html');
    }
}


/**
 * youtubeの動画をダウンロードさせるイベント
 */
function onClickDownload() {
    console.log("===== youtube download =====");

    // oading表示
    startLoading();

    // format 取得 (movie or music)
    let formatList = document.getElementById('youtube-download-form').format;
    let format = formatList.value;

    // download
    let url = document.getElementById('url').value;
    download(url, format);

    // submitを実行しない
    return false;
}
