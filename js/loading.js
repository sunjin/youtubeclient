const defaultMessage = "Download";
const loadingMessage = "Downloading...";
const finishedMessage = "Finished";
const errorMessage = "Error...";

const targetId = "btn";

// wait 指定された時間待機する関数
const wait = (sec) => {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, sec * 1000);
        //setTimeout(() => {reject(new Error("エラー！"))}, sec*1000);
    });
};


/**
 * Loading開始
 */
async function startLoading() {
    // btnの「Download」を「Downloading...」に変更
    document.getElementById(targetId).value = loadingMessage;

    // TODO loading animation
}

/**
 * Loading終了
 */
async function stopLoading() {

    try {

        // 3秒だけ finishedと表示
        document.getElementById(targetId).value = finishedMessage;

        await wait(3);

        // defaultに戻す
        document.getElementById(targetId).value = defaultMessage;
    } catch (e) {
        console.log("error:", e);
        alert("描画Error...");
    }

}

/**
 * Error発生時の終了
 */
async function errorLoading() {
    try {
        document.getElementById(targetId).value = errorMessage;
        await wait(2);

        document.getElementById(targetId).value = defaultMessage;
    } catch (e) {
        console.log("error:", e);
        alert("描画Error...");
    }
}


