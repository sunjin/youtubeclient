
/**
 * postAjax post通信
 * 
 * @param {string} url 
 * @param {string} json request body
 * @param {function} callback 
 * @param {string} userID 
 * @param {string} token 
 */
function postAjax(url, json, callback, userID, token) {

    let xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    xhr.setRequestHeader('Authorization', token);
    xhr.setRequestHeader('User-ID', userID);
    xhr.onload = () => {
        console.log(xhr.status);
        console.log("success!");

        if (xhr.status == 200) {
            callback(xhr);
            return;
        } else {
            console.log("error:", xhr.status);
            alert("Error:" + xhr.status);
        }
    };
    xhr.onerror = () => {
        console.log(xhr.status);
        console.log("error!");
        alert("Server Error:" + xhr.status);
    };
    xhr.send(json);
}

/**
 * getAjax get通信
 * 
 * @param {string} url 
 * @param {function} callback   
 * @param {string} userID 
 * @param {string} token 
 */
function getAjax(url, callback, userID, token) {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.setRequestHeader('Authorization', token);
    xhr.setRequestHeader('User-ID', userID);
    xhr.onload = () => {
        console.log(xhr.status);
        console.log("success!");

        if (xhr.status == 200) {
            callback(xhr);
            return;
        }
        else if (xhr.status == 400) {
            console.log("error:400 許可されていない通信です");
        }
        else if (xhr.status == 401) {
            console.log("error:401 認証が必要なため通信できない状態");
        }
        else if (xhr.status == 403) {
            console.log("error:403 アクセスが禁止されていて通信できない状態");
        }
        else if (xhr.status == 404) {
            console.log("error:404 情報が存在しないために通信できない状態");
        }
        else if (xhr.status == 500) {
            console.log("error:500 サーバー側の不具合で通信できない状態");
        }
        else if (xhr.status == 503) {
            console.log("error:503 サーバーに負荷がかかって通信できない状態");
        } else {
            console.log("error:", xhr.status, "予期せぬエラー");
        }
    };
    xhr.onerror = () => {
        console.log(xhr.status);
        console.log("error!");
        alert("Server Error:" + xhr.status);
    };
    xhr.send();
}